<?php

function brendan_hello_page() {
    return '<p>Hello.</p>';
}

function brendan_hello_arg_page($uid) {
    $uid = (int) $uid;
    $user = user_load($uid);
    dbg($user);exit;
    if ($user) {
        return "<p>hello $user->name</p>";
    }
    return 'Not a valid uid.';
}
